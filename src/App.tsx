import * as React from 'react'
import './App.css'

import Header from '@components/Header/Header'
import TodoList from '@components/TodoList/TodoList'
import { injectGlobal } from 'styled-components'

interface IProps {}

/**
 * Component
 */
class App extends React.Component<IProps> {

  constructor (props: IProps) {
    super(props)
  }

  /**
   * render
   */
  public render () {
    return (
      <div>
        <Header text='TODO' />
        <TodoList />
      </div>
    )
  }
}

export default App

injectGlobal`
  html {
    font-family: "";
    line-height: 1.5;
    font-size: 15px;
    body {
      margin: 30px;
      padding: 0;
    }
  }
`
