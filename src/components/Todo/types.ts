export interface IProps {
  id: number
  name: string
  status: boolean
  changeStatus: (todo: IState) => any
}

export interface IState {
  id: number
  name: string
  status: boolean
}
