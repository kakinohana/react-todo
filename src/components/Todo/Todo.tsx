import { IProps, IState } from '@components/Todo/types'
import * as React from 'react'
import styled, { css } from 'styled-components'

/**
 * Component
 */
class Todo extends React.Component<IProps, IState> {
  constructor (props: IProps) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  /**
   * render
   */
  public render () {
    const { children, ...rest } = this.props
    return (
      <TodoLi status={rest.status} onClick={this.handleClick}>
        {this.props.name}
      </TodoLi>
    )
  }

  /**
   * handleClick
   */
  private handleClick () {
    return this.props.changeStatus(this.props)
  }
}

export default Todo

const TodoLi = styled.li <{
  status: IProps['status']
}>`
  margin-bottom: 5px
  cursor: pointer

  ${({ status }) =>
  status &&
  css`
    text-decoration: line-through
  `
}
`
