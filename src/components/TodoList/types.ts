import { IProps as ITodoProps } from '@components/Todo/types'
export interface ITodoListProps {
  todos: Array<{
    id: ITodoProps['id']
    name: ITodoProps['name']
    status: ITodoProps['status']
  }>
}
