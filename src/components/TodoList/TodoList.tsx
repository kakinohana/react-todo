import Form from '@components/Form/Form'
import { IProps as IFormProps } from '@components/Form/types'
import Todo from '@components/Todo/Todo'
import { IProps as ITodoProps } from '@components/Todo/types'
import { ITodoListProps } from '@components/TodoList/types'
import * as React from 'react'
import styled from 'styled-components'

interface IProps {}

interface IState {
  todos: ITodoListProps['todos']
  countTodo: number
  defaultFormText: string
}

/**
 * Component
 */
class TodoList extends React.Component<IProps, IState> {
  constructor (props: IProps) {
    super(props)
    const todos = [
      { id: 1, name: 'todo1', status: false },
      { id: 2, name: 'todo2', status: true },
      { id: 3, name: 'todo3', status: true },
      { id: 4, name: 'todo4', status: false }
    ]
    this.state = {
      todos,
      countTodo: todos.length + 1,
      defaultFormText: ''
    }
    this.changeTodoStatus = this.changeTodoStatus.bind(this)
    this.addTodo = this.addTodo.bind(this)
  }

  /**
   * changeTodoStatus
   */
  public changeTodoStatus (clickTodo: ITodoProps) {
    const todos = this.state.todos.slice()
    const todo = todos[clickTodo.id - 1]
    todo.status = !todo.status
    todos[clickTodo.id - 1] = todo

    this.setState(
      { todos })
  }

  /**
   * addTodo
   */
  public addTodo (form: IFormProps) {
    const todos = this.state.todos.slice()
    todos.push({
      id: this.state.countTodo,
      name: form.text,
      status: false
    })
    this.setState({ todos })
    this.setState({
      countTodo: this.state.countTodo + 1
    })
  }

  /**
   * render
   */
  public render () {
    return (
      <div>
        <TodoUl>
          {this.state.todos.map((todo: ITodoProps, index: number) =>
            <Todo key={todo.id} {...todo}
            changeStatus={this.changeTodoStatus}
              />
          )}
        </TodoUl>
        <Form
          text={this.state.defaultFormText}
          addText={this.addTodo}
          // styled
          placeholderColor={'#8f8f8f'}
          inputWidth={250}
          buttonWidth={50} />
      </div>
    )
  }
}
export default TodoList

const TodoUl = styled.ul`
  padding: 0;
  margin: 0;
  padding-left: 1em;
  margin-bottom: 20px;
`
