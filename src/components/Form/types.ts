export interface IProps {
  text: string
  inputWidth: number
  buttonWidth: number
  placeholderColor: string
  addText: (form: IState) => any
}

export interface IState {
  text: string
}
