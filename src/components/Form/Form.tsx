import { IProps as IFormProps, IState as IFormState } from '@components/Form/types'
import * as React from 'react'
import styled from 'styled-components'

/**
 * Component
 */
class Form extends React.Component<IFormProps, IFormState> {
  constructor (props: IFormProps) {
    super(props)
    this.state = {
      text: this.props.text
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  /**
   * render
   */
  public render () {
    return (
        <div className='form'>
            <form>
              <Input
                name='name'
                type='text'
                placeholder={'タスク内容'}
                value={this.state.text}
                onChange={this.handleChange}
                // styled
                placeholderColor={this.props.placeholderColor}
                width={this.props.inputWidth} />
              <Button width={this.props.buttonWidth} onClick={this.handleClick} >追加</Button>
            </form>
        </div >
    )
  }

  /**
   * handleChange
   */
  public handleChange (e: React.FormEvent<HTMLInputElement>) {

    this.setState({
      text: (e.target as HTMLInputElement).value
    })
  }

  /**
   * handleClick
   */
  private handleClick (e: React.FormEvent<HTMLButtonElement>) {
    e.preventDefault()
    return this.props.addText(this.state)
  }
}
export default Form

const Input = styled.input <{
  width: IFormProps['inputWidth']
  placeholderColor: IFormProps['placeholderColor']
}>`
  appearance: none;
  height: 30px;
  border: 1px solid black;
  padding: 0 10px;
  box-sizing: border-box;
  vertical-align: bottom;
  font-size: 13px;
  width: ${({ width }) => width}px;
  color: ${({ placeholderColor }) => placeholderColor}
`
const Button = styled.button <{
  width: IFormProps['buttonWidth']
}>`
  appearance: none;
  height: 30px;
  border: 1px solid black;
  padding: 0 10px;
  box-sizing: border-box;
  vertical-align: bottom;
  font-size: 13px;
  width: ${({ width }) => width}px;
`
